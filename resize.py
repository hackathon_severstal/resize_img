import cv2, numpy

original_image = cv2.imread('4.jpg',1)
original_height, original_width = original_image.shape[:2]
factor = 0.4
resized_image = cv2.resize(original_image, (int(original_height*factor), int(original_width*factor)), interpolation=cv2.INTER_CUBIC )

cv2.imwrite('resized_image.jpg',resized_image)